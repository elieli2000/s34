// EXPRESS SETUP
// 1. Import by using the 'require' directive to get access to the components of express package/depency
const express = require('express')
// 2. Use the express() function and assign in to an app variable  tp create an express app or app server
const app = express()
// 3. Declare a variable for the port of the server
const port = 3000

// Middlewares
// These two .use are essential in express
// Allows your app to reas json format data
app.use(express.json())
// Allows your app to read data from forms.
app.use(express.urlencoded({extended: true}))

// Routes

// Get request route
app.get('/', (request, response) => {
	// once the route is accessed it will then send a string response conataining "Hello World"
	response.send('Hello World')
})

app.get('/hello', (request, response) => {
	response.send("Hello from /hello endpoint!")
})

// Register user route

// mock database
let users = [];

app.post('/register', (request, response) => {
	if(request.body.username !== " " && request.body.password !== " ") {
		users.push(request.body)
		console.log(users)
		response.send(`User ${request.body.username} succeddfully registered`)
	} else {
		response.send('Please input BOTH username and password')
	}
})




app.listen(port, () => console.log(`Server is running at port ${port}`))